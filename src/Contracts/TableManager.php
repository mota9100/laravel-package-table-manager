<?php

namespace  Mota\TableManager\Contracts;

use Illuminate\Database\Eloquent\Model;

interface TableManager {

    public function Insert(array $inputs);

    public function InsertGetID(array $inputs);

    public function UpdateWithID($id, array $inputs);

    public function CustomUpdate(array $where, array $inputs);

    public function CustomDelete(array $where);

    public function SelectFirst(array $where = null);

    public function SelectGet(array $where = null);

    public function GetCount(array $where = null);

    public function SelectLast(array $where = null);

    public function GetNewModel();

    public function GetConnection();

    public function GetTableName();

    public function GetModelName();

    public function GetTablePrimaryKey();
}