<?php

namespace Mota\TableManager\Tables;

use Mota\TableManager\TableManager;
use Mota\TableManager\Models\TestPost;

class TestTablePost extends TableManager {

    protected $tableManager;

    public function __construct($connection = null) {

        Parent::__construct(TestPost::class, $connection);
    }

}