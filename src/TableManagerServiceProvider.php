<?php

namespace Mota\TableManager;

use Illuminate\Support\ServiceProvider;
use Mota\TableManager\src\Console\ModelMakeCommand;
use Mota\TableManager\src\Console\TableMakeCommand;

class TableManagerServiceProvider extends ServiceProvider {

    public function boot() {

        $this->commands([
            TableMakeCommand::class,
            ModelMakeCommand::class
        ]);

        $this->publishes([
            __DIR__ . '/Migrations' => database_path('migrations')
        ]);
    }

    public function register() {

        $this->app->singleton('command.tablemanager.make', function ($app) {

            return new TableMakeCommand($app['files']);
        });
    }
}