<?php

namespace Mota\TableManager\Tests\Feature;

use Mota\TableManager\Tests\TestCase;
use Mota\TableManager\Models\TestPost;
use Mota\TableManager\Tables\TestTablePost;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Collection;


class TestTablePostTest extends TestCase {

    protected $defaultConnection = 'default';
    protected $testTable;
    protected $testModel;
    protected $connection;
    protected $database;

    public function setUp() {

        parent::setUp();
        $this->testTable = TestTablePost::class;
        $this->testModel = TestPost::class;
        $this->connection = config("database.$this->defaultConnection", '');
        $databaseMap = "database.connections." . $this->connection . ".database";
        $this->database = config($databaseMap, '');
    }

    public function testInsert() {

        $objTable = new $this->testTable($this->connection);

        $booleanInsert = $objTable->Insert([
            'postid' => 1,
            'title' => 'post one title',
            'text' => 'post one test'
        ]);

        $this->assertTrue($booleanInsert);

        $tableName = $objTable->GetTableName();

        $this->assertDatabaseHas("$this->database.$tableName", [
            'title' => 'post one title'
        ], $this->connection);
    }

    public function testInsertId() {

        $objTable = new $this->testTable($this->connection);

        $idInsert = $objTable->InsertGetID([
            'postid' => 2,
            'title' => 'post two title',
            'text' => 'post two test'
        ]);

        $this->assertContainsOnly('integer', [$idInsert]);
        $this->assertContains(2, [$idInsert]);

        $tableName = $objTable->GetTableName();

        $this->assertDatabaseHas("$this->database.$tableName", [
            'title' => 'post two title'
        ], $this->connection);
    }

    public function testSelectGet() {

        $objTable = new $this->testTable($this->connection);

        $selectGet = $objTable->SelectGet();

        $this->assertInstanceOf(Collection::Class, $selectGet);
        $eloquentCollectionSelectGet = $selectGet->toArray();

        $this->assertCount(2, $eloquentCollectionSelectGet);

        $filteredSelectGet = $objTable->SelectGet([
            ['postid', '=', 1]
        ]);

        $this->assertInstanceOf(Collection::Class, $filteredSelectGet);
        $eloquentCollectionFilteredSelectGet = $filteredSelectGet->toArray();

        $this->assertCount(1, $eloquentCollectionFilteredSelectGet);

        $nonExistSelectGet = $objTable->SelectGet([
            ['postid', '=', 111]
        ]);

        $this->assertInstanceOf(Collection::Class, $nonExistSelectGet);
        $eloquentCollectionNonExistSelectGet = $nonExistSelectGet->toArray();

        $this->assertCount(0, $eloquentCollectionNonExistSelectGet);
    }

    public function testSelectFirst() {

        $objTable = new $this->testTable($this->connection);

        $tablePrimaryKey = $objTable->GetTablePrimaryKey();

        $select = $objTable->SelectFirst();

        $this->assertInstanceOf($this->testModel, $select);

        $selectId = $select->$tablePrimaryKey;

        $this->assertContainsOnly('integer', [$selectId]);
        $this->assertContains(1, [$selectId]);

        $filteredSelect = $objTable->SelectFirst([
            ['postid', '=', 1]
        ]);

        $this->assertInstanceOf($this->testModel, $filteredSelect);

        $filteredSelectId = $filteredSelect->$tablePrimaryKey;

        $this->assertContainsOnly('integer', [$filteredSelectId]);
        $this->assertContains(1, [$filteredSelectId]);

        $nonExistSelect = $objTable->SelectFirst([
            ['postid', '=', 111]
        ]);

        $this->assertNull($nonExistSelect);
    }

    public function testSelectLast() {

        $objTable = new $this->testTable($this->connection);

        $tablePrimaryKey = $objTable->GetTablePrimaryKey();

        $select = $objTable->SelectLast();

        $this->assertInstanceOf($this->testModel, $select);

        $selectId = $select->$tablePrimaryKey;

        $this->assertContainsOnly('integer', [$selectId]);
        $this->assertContains(2, [$selectId]);

        $filteredSelect = $objTable->SelectLast([
            ['postid', '=', 2]
        ]);

        $this->assertInstanceOf($this->testModel, $filteredSelect);

        $filteredSelectId = $filteredSelect->$tablePrimaryKey;

        $this->assertContainsOnly('integer', [$filteredSelectId]);
        $this->assertContains(2, [$filteredSelectId]);

        $nonExistSelect = $objTable->SelectLast([
            ['postid', '=', 222]
        ]);

        $this->assertNull($nonExistSelect);
    }

    public function testCustomUpdate() {

        $objTable = new $this->testTable($this->connection);

        $update = $objTable->CustomUpdate([
            ['title', '=', 'post one title']
        ], [
            'title' => 'post one title updated'
        ]);

        $this->assertTrue($update);

        $nonEffectUpdate = $objTable->CustomUpdate([
            ['title', '=', 'post one title updated']
        ], [
            'title' => 'post one title updated'
        ]);

        $this->assertFalse($nonEffectUpdate);

        $nonExistUpdate = $objTable->CustomUpdate([
            ['title', '=', 'post three title']
        ], [
            'title' => 'post three title updated'
        ]);

        $this->assertFalse($nonExistUpdate);


        $tableName = $objTable->GetTableName();

        $this->assertDatabaseHas("$this->database.$tableName", [
            'title' => 'post one title updated'
        ], $this->connection);
    }

    public function testUpdateWithID() {

        $objTable = new $this->testTable($this->connection);

        $update = $objTable->UpdateWithID(2, [
            'title' => 'post two title updated'
        ]);

        $this->assertTrue($update);

        $nonEffectUpdate = $objTable->UpdateWithID(2, [
            'title' => 'post two title updated'
        ]);

        $this->assertFalse($nonEffectUpdate);

        $nonExistUpdate = $objTable->UpdateWithID(3, [
            'title' => 'post three title updated'
        ]);

        $this->assertFalse($nonExistUpdate);


        $tableName = $objTable->GetTableName();

        $this->assertDatabaseHas("$this->database.$tableName", [
            'title' => 'post two title updated'
        ], $this->connection);
    }

    public function testGetCount() {

        $objTable = new $this->testTable($this->connection);

        $totalCount = $objTable->GetCount();

        $this->assertContainsOnly('integer', [$totalCount]);
        $this->assertContains(2, [$totalCount]);

        $filteredCount = $objTable->GetCount([
            ['title', '=', 'post one title updated']
        ]);

        $this->assertContainsOnly('integer', [$filteredCount]);
        $this->assertContains(1, [$filteredCount]);

        $nonExistCount = $objTable->GetCount([
            ['title', '=', 'post three title']
        ]);

        $this->assertContainsOnly('integer', [$nonExistCount]);
        $this->assertContains(0, [$nonExistCount]);
    }

    public function testDelete() {

        $objTable = new $this->testTable($this->connection);

        $deleteExist = $objTable->CustomDelete([
            ['title', '=', 'post one title updated']
        ]);

        $this->assertTrue($deleteExist);

        $nonExistDelete = $objTable->CustomDelete([
            ['title', '=', 'post one title updated']
        ]);

        $this->assertFalse($nonExistDelete);

        $tableName = $objTable->GetTableName();

        $this->assertDatabaseMissing("$this->database.$tableName", [
            'title' => 'post one title updated'
        ], $this->connection);
    }
}