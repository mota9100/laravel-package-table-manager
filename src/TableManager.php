<?php

namespace Mota\TableManager;

use Mota\TableManager\Contracts\TableManager as TableManagerInterface;
use Mota\TableManager\Utilities\ModelEngine;

class TableManager extends ModelEngine implements TableManagerInterface {

    private $modelName;

    private $connection;

    /**
     * TableManager constructor.
     * @param string $model
     * @param string|null $connection
     */
    function __construct(string $model, $connection) {

        $arrayOfModel = explode('\\', $model);
        $defaultConnection = config('database.default');

        $this->modelName = end($arrayOfModel);
        $this->connection = (!is_null($connection)) ? $connection : $defaultConnection;

        parent::__construct($model, $this->connection);
    }

    /**
     * @param string $key
     * @return null|string
     */
    protected function GetProperty(string $key) {

        return (property_exists($this, $key)) ?
            $this->$key :
            null;
    }

    /**
     * @param array $inputs
     * @return bool
     */
    public function Insert(array $inputs) {

        $action = $this->Create($inputs);

        $actionModel = $action->GetModelInstance();

        $this->Disconnect($actionModel);

        return $action->GetProperty('wasRecentlyCreated');
    }

    /**
     * @param array $inputs
     * @return null|int
     */
    public function InsertGetID(array $inputs) {

        $action = $this->Create($inputs);

        $actionModel = $action->GetModelInstance();

        $this->Disconnect($actionModel);

        $createStatus = $action->GetProperty('wasRecentlyCreated');

        $primaryKey = $this->primaryKey;

        return ($createStatus) ? $actionModel->$primaryKey : null;
    }

    /**
     * @param int $id
     * @param array $inputs
     * @return bool
     */
    public function UpdateWithID($id, array $inputs) {

        $action = $this->Update([
            [$this->primaryKey, '=', $id]
        ], $inputs);

        $actionModel = $action->GetModelInstance();

        $this->Disconnect($actionModel);

        $updateStatus = $action->GetProperty('wasRecentlyUpdated');

        return $updateStatus;
    }

    /**
     * @param array $where
     * @param array $inputs
     * @return bool
     */
    public function CustomUpdate(array $where, array $inputs) {

        $action = $this->Update($where, $inputs);

        $actionModel = $action->GetModelInstance();

        $this->Disconnect($actionModel);

        $updateStatus = $action->GetProperty('wasRecentlyUpdated');

        return $updateStatus;
    }

    /**
     * @param array $where
     * @return bool
     */
    public function CustomDelete(array $where) {

        $action = $this->Delete($where);

        $actionModel = $action->GetModelInstance();

        $this->Disconnect($actionModel);

        $updateStatus = $action->GetProperty('wasRecentlyDeleted');

        return $updateStatus;
    }

    /**
     * @param array|null $where
     * @return mixed
     */
    public function SelectFirst(array $where = null) {

        $action = $this->Read($where);

        $actionModel = $action->GetModelInstance();

        $this->Disconnect($actionModel);

        return $actionModel->first();
    }

    /**
     * @param array|null $where
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function SelectGet(array $where = null) {

        $action = $this->Read($where);

        $actionModel = $action->GetModelInstance();

        $this->Disconnect($actionModel);

        return $actionModel->get();
    }

    /**
     * @param array|null $where
     * @return int
     */
    public function GetCount(array $where = null) {

        $action = $this->Read($where);

        $actionModel = $action->GetModelInstance();

        $this->Disconnect($actionModel);

        return $actionModel->count();
    }

    /**
     * @param array|null $where
     * @return mixed
     */
    public function SelectLast(array $where = null) {

        $action = $this->Read($where);

        $actionModel = $action->GetModelInstance();

        $this->Disconnect($actionModel);

        return $actionModel->orderBy($this->primaryKey, 'desc')->first();
    }

    /**
     * @return string|null
     */
    public function GetModelName() {

        return $this->modelName;
    }

    /**
     * @return string|null
     */
    public function GetConnection() {

        return $this->connection;
    }


    protected function GetCombo($collection = [], $identity = null, $search = null, $pageCurent = null, $pageCount = null, $valueKey = null) {

        $where = [];
        $query = $this->GetNewModel();
        $primaryKey = $this->primaryKey;

        if(is_array($collection)) {

            if(key_exists('name', $collection) && key_exists('id', $collection)) {

                $where[] = [$collection['name'], '=', $collection['id']];
            }
        }

        if($identity != '' && !is_null($identity)) {

            $where[] = [$primaryKey, '=', $identity];
        }

        $query = $query->where($where);

        if($search != '' && !is_null($search)) {

            $query = $query->where(function ($query) use ($search) {

                for ($indexColumn = 0; count($this->comboColumns) > $indexColumn; $indexColumn++) {

                    if($indexColumn == 0) {

                        $query->where([
                            [$this->comboColumns[$indexColumn], 'like', "%$search%"]
                        ]);

                    } else {

                        $query->orWhere([
                            [$this->comboColumns[$indexColumn], 'like', "%$search%"]
                        ]);
                    }
                }
            });

        }

        $count = $query->count();

        if(!is_null($pageCurent) && !is_null($pageCount)) {

            $skip = ($pageCurent - 1) * $pageCount;
            $query = $query->skip($skip)->take($pageCount);
        }

        $gets = $query->get();

        $return = [
            'results' => [],
            'count' => $count
        ];

        foreach ($gets as $get) {

            $text = '';

            foreach ($this->comboColumns as $comboColumn) {

                $text .= ' ' .$get->$comboColumn;
            }

            $return['results'][] = [
                'id' => (!is_null($valueKey)) ? $get->$valueKey : $get->$primaryKey,
                'text' => $text
            ];
        }


        return (object) [
            'Result' => $return['results'],
            'Pagination' => [
                'PageCount' => $pageCount,
                'PageCurrent' => $pageCurent,
                'TotalCount' => $return['count']
            ]
        ];
    }

}