<?php

namespace Mota\TableManager\Facades;

use Illuminate\Support\Facades\Facade;

class TableManager extends Facade {

    protected static function getFacadeAccessor() {

        return 'tablemanager';
    }
}