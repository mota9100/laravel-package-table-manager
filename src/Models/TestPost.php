<?php

namespace Mota\TableManager\Models;

use Illuminate\Database\Eloquent\Model;

class TestPost extends Model {

    public function __construct(array $attributes = [], $connection = null) {

        if(!is_null($connection)) {

            $this->connection = $connection;
        }

        parent::__construct($attributes);
    }

    protected $table = 'tb_table_manager_posts';

    protected $primaryKey = 'PostID';

    protected $fillable = [];

    protected $hidden = [];

    public $timestamps = true;
}