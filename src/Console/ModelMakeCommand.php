<?php
/**
 * Created by PhpStorm.
 * User: mota
 * Date: 29/05/18
 * Time: 11:00
 */

namespace Mota\TableManager\src\Console;


use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;
use InvalidArgumentException;

class ModelMakeCommand extends GeneratorCommand
{
    protected $name = 'make:tablemanagermodel';


    protected $description = 'Create a new table model class';


    protected $type = 'TableManagerModel';


    protected function getStub() {

        return __DIR__ . '/Stubs/tablemodel.stub';
    }

    protected function getDefaultNamespace($rootNamespace) {

        return $rootNamespace;
    }

    protected function buildClass($name) {

        $this->parseModel($name);

        $arrayName = explode('\\', $name);
        $modelName = end($arrayName);
        $modelNameSpace = str_replace('\\'.$modelName, '', $name);

        $replace = [
            'DummyModelNameSpace' => $modelNameSpace
        ];

        $infoText = "Set necessary details of model {$name}\n" .
            'protected $table;' . "\n" .
            'protected $primaryKey;' . "\n" .
            'public $timestamps;';

        $this->warn($infoText);
        return str_replace(
            array_keys($replace), array_values($replace), parent::buildClass($name)
        );
    }

    protected function parseModel($model) {
        if (preg_match('([^A-Za-z0-9_/\\\\])', $model)) {
            throw new InvalidArgumentException('Model name contains invalid characters.');
        }

        $model = trim(str_replace('/', '\\', $model), '\\');

        if (! Str::startsWith($model, $rootNamespace = $this->laravel->getNamespace())) {
            $model = $rootNamespace.$model;
        }

        return $model;
    }
}