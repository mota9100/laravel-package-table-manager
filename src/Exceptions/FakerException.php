<?php
/**
 * Created by PhpStorm.
 * User: mota
 * Date: 02/06/18
 * Time: 01:09
 */

namespace Mota\TableManager\Exceptions;


use Mota\TableManager\Exceptions\TableManagerException;
use Throwable;

class FakerException extends TableManagerException {

    public function __construct(string $message = "", int $code = 0, string $file = null, string $line = null, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->file = $file;
        $this->line = $line;
    }
}