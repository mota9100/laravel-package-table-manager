<?php
/**
 * Created by PhpStorm.
 * User: mota
 * Date: 01/06/18
 * Time: 23:17
 */

namespace Mota\TableManager\Exceptions;


use Throwable;

class ColumnNotFoundException extends TableManagerException {

    public function __construct(string $message = "", int $code = 0, string $file = null, string $line = null, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->file = $file;
        $this->line = $line;
    }
}