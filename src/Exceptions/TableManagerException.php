<?php
/**
 * Created by PhpStorm.
 * User: mota
 * Date: 30/05/18
 * Time: 11:55
 */

namespace Mota\TableManager\Exceptions;


use Throwable;

class TableManagerException extends \Exception {

    public function __construct(string $message = "", int $code = 0, string $file = null, string $line = null, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->file = $file;
        $this->line = $line;
    }
}