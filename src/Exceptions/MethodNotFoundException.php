<?php
/**
 * Created by PhpStorm.
 * User: mota
 * Date: 30/05/18
 * Time: 12:01
 */

namespace Mota\TableManager\Exceptions;


use Throwable;

class MethodNotFoundException extends TableManagerException {

    public function __construct(string $message = "", int $code = 0, string $file = null, string $line = null, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->file = $file;
        $this->line = $line;
    }
}