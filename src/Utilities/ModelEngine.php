<?php

namespace Mota\TableManager\Utilities;

use Illuminate\Support\Facades\Schema;

class ModelEngine {

    private $connection;

    protected $modelNameSpace;

    protected $primaryKey;

    protected $tableName;

    protected $columns = [];

    protected $columnsTypes = [];

    protected $comboColumns = [];

    function __construct($model, $connection) {

        $this->modelNameSpace = $model;
         $this->connection = $connection;

         $objModel = new $model([], $connection);
         $this->primaryKey = $objModel->getKeyName();
         $this->tableName = $objModel->getTable();

         $this->columns = Schema::getColumnListing($this->tableName);

         foreach ($this->columns as $column) {
             $this->columnsTypes[$column] = Schema::getColumnType($this->tableName, $column);
         }
    }

    /**
     * @return mixed
     */
    private function Connect() {

         return new $this->modelNameSpace([], $this->connection);
    }

    /**
     * @param array $inputs
     * @return array
     */
    private function ValidInputs(array $inputs) {

        $newInputs = [];

        foreach ($inputs as $key => $value) {

            $newInputs[strtolower($key)] = $value;
        }

        return $newInputs;
    }

    /**
     * @param $model
     */
    protected function Disconnect($model) {

        $model->getConnection()->disconnect();
    }

    /**
     * @param array|null $where
     * @return \Mota\TableManager\Utilities\ActionsResponse mixed
     */
    protected function Read(array $where = null) {

        $connection = $this->Connect();

        if ($where) {

            $connection = $connection->where($where);
        }

        return (new ActionsResponse())->CreateActionResponse($connection);
    }

    /**
     * @param array $inputs
     * @return \Mota\TableManager\Utilities\ActionsResponse mixed
     */
    protected function Create(array $inputs) {

        $connection = $this->Connect();

        $inputs = $this->ValidInputs($inputs);

        foreach ($this->columns as $column) {

            $inputKey = strtolower($column);

            if (key_exists($inputKey, $inputs)) {

                $connection->$column = $inputs[$inputKey];
            }
        }

        $connection->save();

        return (new ActionsResponse())->CreateActionResponse($connection, [
            'wasRecentlyCreated' => ((isset($connection->wasRecentlyCreated)) ? $connection->wasRecentlyCreated : false)
        ]);
    }

    /**
     * @param array $where
     * @param array $inputs
     * @return \Mota\TableManager\Utilities\ActionsResponse mixed
     */
    protected function Update(array $where, array $inputs) {

        $inputs = $this->ValidInputs($inputs);

        $validInputs = [];

        foreach ($this->columns as $column) {

            $inputKey = strtolower($column);

            if (key_exists($inputKey, $inputs)) {

                $validInputs[$inputKey] = $inputs[$inputKey];
            }
        }

        $connection = $this->Read($where)->GetModelInstance();

        $update = $connection->update($validInputs);

        return (new ActionsResponse())->CreateActionResponse($connection, [
            'wasRecentlyUpdated' => (($update) ? true : false)
        ]);
    }

    /**
     * @param array $where
     * @return \Mota\TableManager\Utilities\ActionsResponse mixed
     */
    protected function Delete(array $where) {

        $connection = $this->Read($where)->GetModelInstance();

        $delete = $connection->delete();

        return (new ActionsResponse())->CreateActionResponse($connection, [
            'wasRecentlyDeleted' => (($delete) ? true : false)
        ]);
    }

    protected function GetColumnsInLower() {

        $inLower = [];
        foreach ($this->columns as $column) {

            $inLower[] = strtolower($column);
        }

        return $inLower;
    }

    /**
     * @param array $replaceColumns
     * @param array $deleteColumns
     * @return array
     * @throws \Mota\TableManager\Exceptions\ColumnNotFoundException
     * @throws \Mota\TableManager\Exceptions\FakerException
     * @throws \Mota\TableManager\Exceptions\TableManagerException
     */
    public function FakeColumns(array $replaceColumns = [], array $deleteColumns = []) {

        $objColumnFaker = new ColumnFaker();

        $fakeColumns = [];

        foreach ($this->columns as $column) {

            $fakeColumns[strtolower($column)] = $objColumnFaker->FakeColumnValue($this->columnsTypes[$column], $column);
        }

        $columnsInLower = $this->GetColumnsInLower();

        foreach ($deleteColumns as $deleteColumn) {

            $inLower = strtolower($deleteColumn);

            if(in_array($inLower, $columnsInLower)) {

                if(key_exists($inLower, $fakeColumns))
                    unset($fakeColumns[$inLower]);
            }
        }

        $cleanReplaceColumns = [];

        foreach ($replaceColumns as $replaceColumnKey => $replaceColumnValue) {

            $keyInLower = (is_string($replaceColumnKey)) ? strtolower($replaceColumnKey) : $replaceColumnKey;

            if(in_array($keyInLower, $columnsInLower)) {

                $cleanReplaceColumns[$keyInLower] = $replaceColumnValue;
            }
        }

        $fakeColumns = array_merge($fakeColumns, $cleanReplaceColumns);

        return $fakeColumns;
    }

    /**
     * @return mixed
     */
    public function GetNewModel() {

        return $this->Read()->GetModelInstance();
    }

    /**
     * @return string|null
     */
    public function GetTableName() {

        return $this->tableName;
    }

    /**
     * @return string|null
     */
    public function GetModelNameSpace() {

        return $this->modelNameSpace;
    }

    /**
     * @return array
     */
    public function GetColumns() {

        return $this->columns;
    }

    /**
     * @param string $column
     * @return string|null
     */
    public function GetColumnType(string $column) {

        return (key_exists($column, $this->columnsTypes)) ? $this->columnsTypes[$column] : null;
    }

    /**
     * @return array
     */
    public function GetColumnsType() {

        return $this->columnsTypes;
    }

    /**
     * @return string|null
     */
    public function GetTablePrimaryKey() {

        return $this->primaryKey;
    }
}