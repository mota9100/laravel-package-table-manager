<?php
return [
    'id' => [
        "accept" => 'string|integer',
        "change" => 'integer'
    ],
    'firstname' => [
        "accept" => 'string',
        "change" => 'firstNameMale'
    ],
    'lastname' => [
        "accept" => 'string',
        "change" => 'lastName'
    ],
    'name' => [
        "accept" => 'string',
        "change" => 'name'
    ],
    'email' => [
        "accept" => 'string',
        "change" => 'email'
    ],
    'mobile' => [
        "accept" => 'string|integer',
        "change" => 'mobileNumber'
    ],
    'username' => [
        "accept" => 'string',
        "change" => 'firstNameMale'
    ],
    'password' => [
        "accept" => 'string',
        "change" => 'secretHash'
    ],
    'token' => [
        "accept" => 'string',
        "change" => 'token'
    ],
    'cookie' => [
        "accept" => 'string',
        "change" => 'token'
    ],
    'ip' => [
        "accept" => 'string',
        "change" => 'ipv4'
    ],
    'created_at' => [
        "accept" => 'string|datetime|date',
        "change" => 'datetime'
    ],
    'updated_at' => [
        "accept" => 'string|datetime|date',
        "change" => 'datetime'
    ],
    'number' => [
        "accept" => 'string|integer',
        "change" => 'integer'
    ],
    'url' => [
        "accept" => 'string',
        "change" => 'url'
    ],
    'date' => [
        "accept" => 'string|datetime|date',
        "change" => 'date'
    ],
    'status' => [
        "accept" => 'string|integer',
        "change" => 'integer'
    ],
    'amount' => [
        "accept" => 'string|integer',
        "change" => 'bigint'
    ],
    'discount' => [
        "accept" => 'string|integer',
        "change" => 'bigint'
    ],
    'cost' => [
        "accept" => 'string|integer',
        "change" => 'bigint'
    ],
    'percent' => [
        "accept" => 'string|integer',
        "change" => 'percent'
    ],
    'sort' => [
        "accept" => 'string|integer',
        "change" => 'percent'
    ],
    'is' => [
        "accept" => 'string|integer',
        "change" => 'oneZero'
    ],
    'price' => [
        "accept" => 'string|integer',
        "change" => 'bigint'
    ],
    'title' => [
        "accept" => 'string',
        "change" => 'name'
    ],
    'prephone' => [
        "accept" => 'string|integer',
        "change" => 'prePhone'
    ],
    'phone' => [
        "accept" => 'string|integer',
        "change" => 'phoneNumber'
    ],
    'post' => [
        "accept" => 'string|integer',
        "change" => 'postalCode'
    ],
    'year' => [
        "accept" => 'string|integer',
        "change" => 'year'
    ],
    'month' => [
        "accept" => 'string|integer',
        "change" => 'month'
    ],
    'day' => [
        "accept" => 'string|integer',
        "change" => 'day'
    ]
];