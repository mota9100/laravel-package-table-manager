<?php

namespace Mota\TableManager\Utilities;

use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;
use Mota\TableManager\Exceptions\ColumnNotFoundException;
use Mota\TableManager\Exceptions\FakerException;


class FakeValue {

    /**
     * @param $columnType
     * @return int|string
     * @throws ColumnNotFoundException
     * @throws FakerException
     */
    public function GetColumnValue($columnType) {

        $fakerAutoload = __DIR__ .  "/../../../../fzaninotto/faker/src/autoload.php";

        if(!is_file($fakerAutoload)) {

            $fakerAutoload = __DIR__ . "/../../../../../vendor/fzaninotto/faker/src/autoload.php";
        }

        if(!is_file($fakerAutoload))
            throw new FakerException('mota/table-manager require fzaninotto/faker package', 0, __FILE__, __LINE__);

        require_once $fakerAutoload;

        $faker = Faker::create();

        switch ($columnType) {

            case 'integer':

                return $faker->randomDigitNotNull;

                break;

            case 'oneZero':

                return $faker->numberBetween(0, 1);

                break;

            case 'percent':

                return $faker->numberBetween(0, 100);

                break;

            case 'bigint':

                return $faker->numberBetween(1000000000, 99999999999);

                break;

            case 'string':

                return $faker->name;

                break;

            case 'text':

                return $faker->text;

                break;

            case 'boolean':

                return $faker->boolean;

                break;

            case 'json':

                return '{"sample":["sample1"],"json": "jsonfaker"}';

                break;

            case 'blob':

                return $faker->randomHtml(2,3);

                break;

            case 'date':

                return $faker->date($format = 'Y-m-d', $max = 'now');

                break;

            case 'time':

                return $faker->date($format = 'H:i:s', $max = 'now');

                break;

            case 'datetime':

                return $faker->date($format = 'Y-m-d H:i:s', $max = 'now');

                break;

            case 'decimal':

                return $faker->randomDigitNotNull . $faker->randomDigitNotNull . '.' . $faker->randomDigitNotNull;

                break;

            case 'float':

                return $faker->randomFloat();

                break;

            case 'smallint':

                return $faker->numberBetween(1, 99);

                break;

            case 'name':

                return $faker->name;

                break;

            case 'firstNameMale' :

                return $faker->firstNameMale;

                break;

            case 'lastName':

                return $faker->lastName;

                break;

            case 'email':

                return $faker->email;

                break;

            case 'mobileNumber':

                $mobileLength = 11;
                $mobileNum = '';

                for ($index = 0; $index < $mobileLength; $index++) {

                    $mobileNum .= (string) $faker->randomDigitNotNull;
                }

                return $mobileNum;

                break;

            case 'secretHash':

                return Hash::make('secret');

                break;

            case 'token':

                return $faker->sha1;

                break;

            case 'ipv4':

                return $faker->ipv4;

                break;

            case 'url':

                return $faker->url;

                break;

            case'prePhone':

                return '0' . $faker->numberBetween(1, 9) . $faker->numberBetween(1, 9);

                break;

            case 'phoneNumber':

                $phoneLength = 8;
                $phoneNum = '';

                for ($index = 0; $index < $phoneLength; $index++) {

                    $phoneNum .= (string) $faker->randomDigitNotNull;
                }

                return $phoneNum;

                break;

            case "postalCode":

                return $faker->postcode;

                break;

            case "year":

                return $faker->year;

                break;

            case "month":

                return $faker->month;

                break;

            case "day":

                return $faker->dayOfMonth();

                break;

            default:

                throw new ColumnNotFoundException("$columnType Column Type Not Found In mota/table-manager", 4104, __FILE__, __LINE__);

                break;
        }
    }

}