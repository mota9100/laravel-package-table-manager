<?php
/**
 * Created by PhpStorm.
 * User: mota
 * Date: 30/05/18
 * Time: 21:40
 */

namespace Mota\TableManager\Utilities;


class ActionsResponse {

    protected $wasRecentlyCreated = false;

    protected $wasRecentlyUpdated = false;

    protected $wasRecentlyDeleted = false;

    protected $model;

    /**
     * @param string $key
     * @return null|mixed
     */
    public function GetProperty(string $key) {

        return (property_exists($this, $key)) ?
            $this->$key :
            null;
    }

    /**
     * @param $model
     * @return $this
     */
    protected function SetModelInstance($model) {

        $this->model = $model;

        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function GetModelInstance () {

        return $this->GetProperty('model');
    }

    public function CreateActionResponse($model, $properties = []) {

        $thisStatic = new static();

        $thisStatic->model = $model;

        foreach ($properties as $propertyKey => $propertyVlaue) {

            if(property_exists($thisStatic, $propertyKey))
                $thisStatic->$propertyKey = $propertyVlaue;
        }

        return $thisStatic;
    }
}