<?php

namespace Mota\TableManager\Utilities;



class ColumnFaker {


    private $columnTypes = [];

    public function __construct() {

        $this->columnTypes = require_once "ColumnTypes.php";
    }

    /**
     * @param string $columnType
     * @param string $columnName
     * @return int|string
     * @throws \Mota\TableManager\Exceptions\ColumnNotFoundException
     * @throws \Mota\TableManager\Exceptions\FakerException
     */
    public function FakeColumnValue(string $columnType, string $columnName) {

        $columnTypes = (array) $this->columnTypes;

        foreach ($columnTypes as $columnTypeKey => $columnTypeRules) {

            if(preg_match("/$columnTypeKey/", strtolower($columnName))) {

                $accept = $columnTypeRules['accept'];
                $acceptArray = explode('|', $accept);

                if(in_array($columnType, $acceptArray)) {

                    $columnType = $columnTypeRules['change'];
                    break;
                }
            }
        }

        $objFakeValue = new FakeValue();

        return $objFakeValue->GetColumnValue($columnType);
    }
}